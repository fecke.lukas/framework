//
//  Constants.swift
//  Auth01SDK
//
//  Created by Lukáš Fečke on 12/12/2018.
//  Copyright © 2018 Lukáš Fečke. All rights reserved.
//

import Foundation

struct Constants {
    
    struct values {

        static let refreshTokenURL = "/api/v1/refresh-token"
        static let registration = "/registration"
        static let login = "/signin"
        static let contentType = "application/json"
        static let xRequestedWith = "XMLHttpRequest"
        static let bearer = "Bearer"
        
    }
    
    struct parameters {
        
        static let apiKey = "api_key"
        static let apiSecret = "api_secret"
        static let contentType = "Content-Type"
        static let xRequestedWith = "X-Requested-With"
        static let authorization = "Authorization"
        
    }
    
    struct defaults {
        static let refreshToken = "refresh_token"
        static let accessToken = "access_token"
        static let apiKey = "api_key"
        static let endPoint = "end_point"
    }
    
    struct controllers {
        static let webViewController = "WebViewController"
    }
    
    struct storyboards {
        static let webViewControllerStoryboard = "WebViewControllerStoryboard"
    }
}
