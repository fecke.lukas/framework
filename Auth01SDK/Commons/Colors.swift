//
//  Colors.swift
//  Auth01SDK
//
//  Created by Lukáš Fečke on 29/11/2018.
//  Copyright © 2018 Lukáš Fečke. All rights reserved.
//

import UIKit

extension UIColor {
    
    public static let blueGradientColor = UIColor(displayP3Red: 45.0/255.0, green: 137.0/255.0, blue: 239.0/255.0, alpha: 1.0)
    public static let greenGradientColor = UIColor(displayP3Red: 52.0/255.0, green: 176.0/255.0, blue: 67.0/255.0, alpha: 1.0)
    
}
