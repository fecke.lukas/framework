//
//  WebViewNavigationController.swift
//  Auth01SDK
//
//  Created by Lukáš Fečke on 24/01/2019.
//  Copyright © 2019 Lukáš Fečke. All rights reserved.
//

import UIKit

//protocol WebViewNavigationDelegate {
//    func dismiss()
//}

class WebViewController: UIViewController {
    
    @IBOutlet var cancelButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        view.backgroundColor = .white
        
        navigationController?.navigationBar.barTintColor = .clear
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
//        navigationItem.leftBarButtonItem. = UIBarButtonItem()
//        navigationBar.leftBarButtonItem = UIBarButt
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        
//        navigationController?.navigationBar.items = [navigationItem]
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) { [weak self] in
            guard let _self = self else { return }
            _self.cancelButton.isHidden = false
        }
    }
    
    @IBAction func dismiss() {
        super.dismiss(animated: true, completion: nil)
    }
    
    static func instantiate() -> WebViewController {
        return UIStoryboard(name: Constants.storyboards.webViewControllerStoryboard, bundle: Bundle(for: WebViewController.self)).instantiateViewController(withIdentifier: Constants.controllers.webViewController) as! WebViewController
    }
}
