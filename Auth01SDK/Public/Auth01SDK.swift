//
//  Auth01SDK.swift
//  Auth01SDK
//
//  Created by Lukáš Fečke on 16/01/2019.
//  Copyright © 2019 Lukáš Fečke. All rights reserved.
//

import Foundation

public protocol Auth01SDKDelegate {
    func login(success: Bool)
}

open class Auth01SDK {
    
    public static func application(userActivity: NSUserActivity, completion: ((Bool,String) -> Void)?) -> Bool {
        guard let url = userActivity.webpageURL else { return true }
        Auth01SDK.verify(baseURl: url.absoluteString, completion: completion)
        return true
    }
    
    static func verify(baseURl: String, completion: ((Bool,String) -> Void)?) {
        guard checkSDKInitialised() else { return }
        LoginFlow.getAuthTokenWithWebLogin(baseURL: baseURl, completion: completion)
    }
    
    public static func register(completion: ((Bool,String) -> Void)?) {
        guard checkSDKInitialised() else { return }
        guard let endPoint = UserDefaults.standard.value(forKey: Constants.defaults.endPoint) else { return }
        LoginFlow.getAuthTokenWithWebLogin(baseURL: "\(endPoint)\(Constants.values.registration)", completion: completion)
    }
    
    public static func login(completion: ((Bool,String) -> Void)?) {
        guard checkSDKInitialised() else { return }
        guard let endPoint = UserDefaults.standard.value(forKey: Constants.defaults.endPoint) else { return }
        LoginFlow.getAuthTokenWithWebLogin(baseURL: "\(endPoint)\(Constants.values.login)", completion: completion)
    }
    
    private static func checkSDKInitialised() -> Bool {
        guard UserDefaults.standard.value(forKey: Constants.defaults.apiKey) != nil,
            UserDefaults.standard.value(forKey: Constants.defaults.endPoint) != nil else { return false }
        return true
    }
    
    public static func initWith(apiKey: String, endPoint: String) {
        UserDefaults.standard.set(apiKey, forKey: Constants.defaults.apiKey)
        UserDefaults.standard.set(endPoint, forKey: Constants.defaults.endPoint)
        UserDefaults.standard.synchronize()
    }
}


//        Auth01SDK.verify(baseURl: "https://auth.01exchange.io/email-verification/verify/FxRHfHmNzlQew_WsGo-L5OQ7qpIPHatnMgkBfZn3P3GsmBr1XNfQ6fN33Xej4AotrvNj46T3F-LnzXSE4NkYHrOcKGgdC37ZMGzvjxxL-htaHoB2GqjxX77uEa3_OTmtYIJvIRlPfbnwpDDEs5oVVpM3LrOX4l9ESgKBD02F0aHyOyBWkM6FbHp3d0PjqVnrPKnq23BrUSBSaLlKCZAMIu1lzAkhCgegJZ4p5rRepSEXk7uX5ir0b9Lbo3EMZ8kmkMV6kyFHTqt-qi4YpafP_X-boL4xzT2qQwQBljyPOgc")
