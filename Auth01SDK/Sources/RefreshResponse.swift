//
//  RefreshResponse.swift
//  Auth01SDK
//
//  Created by Lukáš Fečke on 17/01/2019.
//  Copyright © 2019 Lukáš Fečke. All rights reserved.
//

import Foundation
import Alamofire

struct RefreshResponse: Decodable {
    
    let accessToken: String
    let refreshToken: String
    let cookieId: String
    let refresh_token: String
    
}
