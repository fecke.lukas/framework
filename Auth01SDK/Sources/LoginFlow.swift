//
//  LoginFlow.swift
//  Auth01SDK
//
//  Created by Lukáš Fečke on 05/12/2018.
//  Copyright © 2018 Lukáš Fečke. All rights reserved.
//

//api_key = `CRs23wYNPbPCOireoMsfSRjavGXr3YT0cr2ltuqVUWrlPyskFVbf424ckvIvCuRR` (edited)
//api_secret = `5ae1b8a17bad4da4fdac796f64c16ecd`
import Foundation
import AuthenticationServices
import Alamofire
import JWTDecode

class LoginFlow {
    
    @available(iOS 12.0, *)
    static func getAuthTokenWithWebLogin(baseURL: String, completion: ((Bool,String) -> Void)? = nil) {
        
        //        let authURL = URL(string: "https://auth.01exchange.io/")
        
        StaticWebView.staticWebView.load(baseURL: baseURL) { token in
            let tokens = token.components(separatedBy: "/")
            print("tokens: \(tokens)")
            print("refresh_token: \(tokens[1])")
            guard tokens.count > 2 else {
                return
            }
            refresh(accessToken: tokens[0], refreshToken: tokens[1])
            guard let completion = completion else { return }
            completion(true, tokens[0])
        }
    }
    
    static func refresh(accessToken: String = "", refreshToken: String = "") {
        
        do {
            let jwt = try decode(jwt: accessToken)
            print("jwt: \(jwt)")
            guard let exp = jwt.body["exp"] as? Double else { return }
            let safetyLimit = 300.0
            let expiration = exp - Date().timeIntervalSince1970 - safetyLimit
            guard expiration > 0 else { return }
            
            print("Refreshing after: \(expiration) seconds")
            
            DispatchQueue.main.asyncAfter(deadline: .now() + expiration) {
                print("Refreshing...")
                let parameters = ["refresh_token": refreshToken]
                guard let apiKey = UserDefaults.standard.value(forKey: Constants.defaults.apiKey) as? String,
                    let endPoint = UserDefaults.standard.value(forKey: Constants.defaults.endPoint) else { return }
                let headers: HTTPHeaders = [
                    Constants.parameters.authorization : "\(Constants.values.bearer) \(accessToken)",
                    Constants.parameters.apiKey: apiKey,
                    Constants.parameters.contentType : Constants.values.contentType,
                    Constants.parameters.xRequestedWith : Constants.values.xRequestedWith
                ]
                
                LoginFlow.getRequest(url: "\(endPoint)\(Constants.values.refreshTokenURL)", parameters: parameters, headers: headers) { data in
                    guard let data = data else {
                        getAuthTokenWithWebLogin(baseURL: "\(endPoint)\(Constants.values.login)")
                        return
                    }
                    do {
                        let response = try JSONDecoder().decode(RefreshResponse.self, from: data)

                        UserDefaults.standard.set(response.accessToken, forKey: Constants.defaults.accessToken)
                        UserDefaults.standard.set(response.refreshToken, forKey: Constants.defaults.refreshToken)
                        UserDefaults.standard.synchronize()
                        refresh(accessToken: response.accessToken, refreshToken: response.refreshToken)
                    } catch {
                        //todo: handle fail here
                        print(error)
                    }
                }
            }
        } catch {
            print(error)
        }
    }
    
    static func getRequest(url: String, _ parameters: [String : Any]? = nil, _ headers: HTTPHeaders? = nil, accessToken: String, _ completion: ((Data?) -> Void)? = nil ) {
        
        LoginFlow.getRequest(url: url, parameters: parameters, headers: headers, completion)
    }
    
    static func getRequest(url: String, parameters: [String : Any]? = [String : Any](), headers: HTTPHeaders?, _ completion: ((Data?) -> Void)? = nil ) {
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            print("Response: \(response)")
            switch response.result {
            case .success:
                guard let json = response.data else {
                    completion?(nil)
                    return
                }
                completion?(json)
            case .failure(let error):
                completion?(nil)
                print("Error:", error)
            }
        }
    }
}
