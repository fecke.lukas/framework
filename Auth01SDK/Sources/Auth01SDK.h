//
//  Auth01SDK.h
//  Auth01SDK
//
//  Created by Lukáš Fečke on 23/11/2018.
//  Copyright © 2018 Lukáš Fečke. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for Auth01SDK.
FOUNDATION_EXPORT double Auth01SDKVersionNumber;

//! Project version string for Auth01SDK.
FOUNDATION_EXPORT const unsigned char Auth01SDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Auth01SDK/PublicHeader.h>
