//
//  AuthButton.swift
//  Auth01SDK
//
//  Created by Lukáš Fečke on 29/11/2018.
//  Copyright © 2018 Lukáš Fečke. All rights reserved.
//

import UIKit
import AuthenticationServices

open class AuthButton: UIButton {
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        
        setTitleColor(.white, for: .normal)
        setTitle("01Auth", for: .normal)
        
        titleLabel?.font = UIFont.systemFont(ofSize: 17.0, weight: .heavy)
        
        //size it
        let screenWidth = UIScreen.main.bounds.width
        //        let screenHeight = UIScreen.main.bounds.height
        
        let width: CGFloat = screenWidth - (2 * 17)
        let height: CGFloat = 50.0
        
        frame = CGRect(x: 17, y: frame.origin.y, width: width, height: height)
        
        //gradient
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = bounds
        gradient.colors = [
            UIColor.greenGradientColor.cgColor,
            UIColor.blueGradientColor.cgColor
        ]

        /* repeat the central location to have solid colors */
//        gradient.locations = [0.0, frame.height, frame.width, 0.0] as [NSNumber]

        /* make it horizontal */
        gradient.startPoint = CGPoint(x: 0, y: 1)
        gradient.endPoint = CGPoint(x: 1, y: 0)

        layer.insertSublayer(gradient, at: 0)

        //round it up
        layer.cornerRadius = 8.0
        layer.masksToBounds = true

        
        addTarget(self, action: #selector(getAuthTokenWithWebLogin), for: .touchUpInside)
    }
    
    @objc func getAuthTokenWithWebLogin() {
        LoginFlow.getAuthTokenWithWebLogin(baseURL: "https://auth.01exchange.io")
    }
}

