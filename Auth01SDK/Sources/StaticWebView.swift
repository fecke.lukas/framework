//
//  WebView.swift
//  TestProject
//
//  Created by Lukáš Fečke on 09/01/2019.
//  Copyright © 2019 Lukáš Fečke. All rights reserved.
//

import UIKit
import WebKit

open class StaticWebView: NSObject {
    
    private var webView: WKWebView?
    private var viewController = WebViewController.instantiate()
    private var completionBlock:  (String) -> Void = { _ in }
    
    private(set) static var staticWebView = StaticWebView()
    
    private var request : URLRequest {
        let baseUrl = "https://auth.01exchange.io/email-verification/verify/FxRHfHmNzlQew_WsGo-L5OQ7qpIPHatnMgkBfZn3P3GsmBr1XNfQ6fN33Xej4AotrvNj46T3F-LnzXSE4NkYHrOcKGgdC37ZMGzvjxxL-htaHoB2GqjxX77uEa3_OTmtYIJvIRlPfbnwpDDEs5oVVpM3LrOX4l9ESgKBD02F0aHyOyBWkM6FbHp3d0PjqVnrPKnq23BrUSBSaLlKCZAMIu1lzAkhCgegJZ4p5rRepSEXk7uX5ir0b9Lbo3EMZ8kmkMV6kyFHTqt-qi4YpafP_X-boL4xzT2qQwQBljyPOgc"
//        let baseUrl = "https://auth.bitpolis.io/"
        let url = URL(string: baseUrl)!
        return URLRequest(url: url)
    }
    
    override private init() {
    }
    
    func load(baseURL: String = "https://auth.01exchange.io/", completionBlock: @escaping (String) -> Void) {
        guard let url = URL(string: baseURL) else { return }
        print("url: \(url.debugDescription)")
        self.completionBlock = completionBlock
        
        /* Now instantiate the web view */
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            guard topController == viewController else {
                
                topController.present(UINavigationController(rootViewController: viewController), animated: true) { [weak self] in
                    guard let _self = self else { return }
                    
                    let preferences = WKPreferences()
                    preferences.javaScriptEnabled = true
                    
                    /* Create a configuration for our preferences */
                    let configuration = WKWebViewConfiguration()
                    configuration.preferences = preferences
                    
                    _self.webView = WKWebView(frame: _self.viewController.view.bounds, configuration: configuration)
                    _self.webView?.backgroundColor = .clear
                    guard let webView = _self.webView else { return }
                    
                    /* Load a web page into our web view */
                    let urlRequest = URLRequest(url: url)
                    webView.load(urlRequest)
                    webView.navigationDelegate = self
//                    webView.uiDelegate = self
                    _self.viewController.view.addSubview(webView)
                }
                return
            }
            
            guard let webView = webView else { return }
            
            /* Load a web page into our web view */
            let urlRequest = URLRequest(url: url)
            webView.load(urlRequest)
        }
    }

    func webView(_ webView: WKWebView, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        guard let hostname = webView.url?.host else {
            return
        }
        
        let authenticationMethod = challenge.protectionSpace.authenticationMethod
        if authenticationMethod == NSURLAuthenticationMethodServerTrust {
            completionHandler(.performDefaultHandling, nil);
        } else {
            completionHandler(.cancelAuthenticationChallenge, nil);
        }
    }
}

extension StaticWebView: WKNavigationDelegate {
    
    public func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Swift.Void)
    {
        decisionHandler(.allow)
        guard let urlString = navigationAction.request.url?.absoluteString else {
            return
        }
        print("urlString: \(urlString)")
        guard !urlString.starts(with: "https://bitpolis.io/") else {
            guard let token = urlString.components(separatedBy: "#").last else {
                return
            }
            print("token: \(token)")
            viewController.dismiss(animated: true) { [weak self] in
                guard let _self = self else { return }
                _self.completionBlock(token)
            }
            return
        }
    }
    
}
